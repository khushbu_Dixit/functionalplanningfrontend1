import { Component, OnInit ,ViewEncapsulation} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare const  myfun:any;
declare const openNav:any;
declare const closeNav:any;
declare const dropdown:any;
@Component({
  selector: 'app-agenda-planning',
  templateUrl: './agenda-planning.component.html',
  styleUrls: ['./agenda-planning.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AgendaPlanningComponent implements OnInit {
  closeResult: string;

  constructor(private modalService: NgbModal) {}
  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }
  ngOnInit(): void {
  }
  open(){
    openNav();
  }
  close(){
    closeNav();
  }
  down(){
    dropdown();
  }
 
}
