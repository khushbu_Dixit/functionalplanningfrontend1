import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaPlanningComponent } from './agenda-planning.component';

describe('AgendaPlanningComponent', () => {
  let component: AgendaPlanningComponent;
  let fixture: ComponentFixture<AgendaPlanningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendaPlanningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
