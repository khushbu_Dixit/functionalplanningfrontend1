import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoginComponent } from './login/login.component';
import { CostElemetsComponent } from './costs/cost-elemets/cost-elemets.component';
import { BuildElementsComponent } from './costs/build-elements/build-elements.component';
import { AddbuildComponent } from './costs/addbuild/addbuild.component';
import { AgendaPlanningComponent } from './agenda-planning/agenda-planning.component';
import { EditAgendaComponent } from './edit-agenda/edit-agenda.component';
import { ApiserviceComponent } from './apiservice/apiservice.component';

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    CostElemetsComponent,
    BuildElementsComponent,
    AddbuildComponent,
    AgendaPlanningComponent,
    EditAgendaComponent,
    ApiserviceComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    NgWizardModule.forRoot(ngWizardConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
