import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbuildComponent } from './addbuild.component';

describe('AddbuildComponent', () => {
  let component: AddbuildComponent;
  let fixture: ComponentFixture<AddbuildComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddbuildComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
