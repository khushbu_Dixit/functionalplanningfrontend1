import { Component, OnInit } from '@angular/core';
declare const  myfun:any;
declare const openNav:any;
declare const closeNav:any;
declare const dropdown:any;
@Component({
  selector: 'app-build-elements',
  templateUrl: './build-elements.component.html',
  styleUrls: ['./build-elements.component.css']
})
export class BuildElementsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  do(){
    myfun();
  }
  open(){
    openNav();
  }
  close(){
    closeNav();
  }
  down(){
    dropdown();
  }

}
