import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildElementsComponent } from './build-elements.component';

describe('BuildElementsComponent', () => {
  let component: BuildElementsComponent;
  let fixture: ComponentFixture<BuildElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuildElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
