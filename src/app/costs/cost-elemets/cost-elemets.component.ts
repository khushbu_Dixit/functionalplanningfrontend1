import { Component, OnInit } from '@angular/core';
declare const  myfun:any;
declare const openNav:any;
declare const closeNav:any;
declare const dropdown:any;
@Component({
  selector: 'app-cost-elemets',
  templateUrl: './cost-elemets.component.html',
  styleUrls: ['./cost-elemets.component.css']
})
export class CostElemetsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  do(){
    myfun();
  }
  open(){
    openNav();
  }
  close(){
    closeNav();
  }
  down(){
    dropdown();
  }

}
