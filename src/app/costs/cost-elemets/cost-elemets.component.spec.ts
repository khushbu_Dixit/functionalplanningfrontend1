import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostElemetsComponent } from './cost-elemets.component';

describe('CostElemetsComponent', () => {
  let component: CostElemetsComponent;
  let fixture: ComponentFixture<CostElemetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostElemetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostElemetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
