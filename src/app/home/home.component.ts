import { Component, OnInit } from '@angular/core';
declare const  myfun:any;
declare const openNav:any;
declare const closeNav:any;
declare const dropdown:any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  do(){
    myfun();
  }
  open(){
    openNav();
  }
  close(){
    closeNav();
  }
  down(){
    dropdown();
  }

}
