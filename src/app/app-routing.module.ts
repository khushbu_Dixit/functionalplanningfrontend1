import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendaPlanningComponent } from './agenda-planning/agenda-planning.component';
import { AddbuildComponent } from './costs/addbuild/addbuild.component';
import { BuildElementsComponent } from './costs/build-elements/build-elements.component';
import { CostElemetsComponent } from './costs/cost-elemets/cost-elemets.component';
import { EditAgendaComponent } from './edit-agenda/edit-agenda.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:"home", component: HomeComponent},
  {path:"",redirectTo:"login",pathMatch:"full"},
  {path:"login",component: LoginComponent},
  {path: "home/costelements", component: CostElemetsComponent},
  {path: "home/buildelements", component: BuildElementsComponent},
  {path: "home/buildelements/addbuild", component:AddbuildComponent},
  {path: "home/agenda", component: AgendaPlanningComponent},
  {path: "home/agenda/edit_agenda", component: EditAgendaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
