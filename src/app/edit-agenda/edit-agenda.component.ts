import { Component, OnInit } from '@angular/core';
declare const  myfun:any;
declare const openNav:any;
declare const closeNav:any;
declare const dropdown:any;
@Component({
  selector: 'app-edit-agenda',
  templateUrl: './edit-agenda.component.html',
  styleUrls: ['./edit-agenda.component.css']
})
export class EditAgendaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  open(){
    openNav();
  }
  close(){
    closeNav();
  }
  down(){
    dropdown();
  }

}
